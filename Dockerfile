FROM gradle:6.0.1-jdk8 as build
WORKDIR /workspace/app
COPY build.gradle .
COPY settings.gradle .
RUN gradle clean check -x test
COPY src src
RUN gradle clean build -x test

# Start with a base image containing Java runtime (mine java 8)
FROM openjdk:8u212-jdk-slim
# Add Maintainer Info
LABEL maintainer="jaroslaw.sikora@britenet.com.pl"
# Add a volume pointing to /tmp
VOLUME /tmp

ARG DEPENDENCY=/workspace/app/build
COPY --from=build ${DEPENDENCY}/libs/*.jar /app/libs/app.jar

ENTRYPOINT ["java","-jar","/app/libs/app.jar"]
